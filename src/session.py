import os, sys
import pandas as pd
import json
import datetime
import copy

import utils
from sculpture import Sculpture

class Session:
	
	def __init__(self, args):
		self.reset(args)

	def reset(self, args):
		self.args = args
		self.blocks = [
			{ 'locked': False, 'sculptId': '', 'pitch': 'c3', 'fx': {} },
			{ 'locked': False, 'sculptId': '', 'pitch': 'a3', 'fx': {} },
			{ 'locked': False, 'sculptId': '', 'pitch': 'g4', 'fx': {} },
			{ 'locked': False, 'sculptId': '', 'pitch': 'd5', 'fx': {} }
		]
		self.instrument = {
			'mode':   { 'play': True, 'compare': False, 'preview': True },
			'blocks': copy.deepcopy(self.blocks)
		}
		self.state = {
			'db':         [],
			'baseModels': [],
			'selectedDbEntry':   0,
			'selectedBaseModel': 0,
			'selectedSculpture': 0,
			'selectedSculpts':   [],
			'sculptInProgress':  False,
			'session': {
				'timestamp':   None,
				'path':        None,
				'sculptures':  [],
				'survey':      {},
				'transcript':  {},
				'annotations': {},
				'instrument':  copy.deepcopy(self.instrument),
				'instrumentLog': []
			},
			'connected': False
		}
		self.sculpture = Sculpture(self.args['sculpture'])

	def restore(self):
		self.getDb()
		self.state['baseModels'] = utils.loadBaseModels(self.args['baseModels'] +'/sessionBaseModels.json')
		self.loadLatestSession()
		return self.state

	def new(self):
		self.reset(self.args)
		self.state['baseModels'] = utils.loadBaseModels(self.args['baseModels'] +'/sessionBaseModels.json')
		timestamp = datetime.datetime.now()
		path, folder = self.makeSessionDirs(timestamp)
		self.state['session'] = {
			'timestamp':   timestamp,
			'path':        path,
			'sculptures':  [],
			'survey':      {},
			'transcript':  {},
			'annotations': {},
			'instrument':  copy.deepcopy(self.instrument),
			'instrumentLog': []
		}
		self.save()
		self.getDb()
		return self.state

	def save(self):
		json_str = json.dumps(self.state['session'], default=str)
		json_file = open(self.state['session']['path'] + '/session.json', "w")
		json_file.write(json_str)
		json_file.close()
		return json_str

	def load(self, session):
		session = self.args['sessions']+session+'/session.json'
		self.state['session'] = json.load(open(session))
		return self.state['session']

	def loadLatestSession(self):
		latestSession = len(self.state['db'])-1
		return self.load(self.state['db'][latestSession])

	def getDb(self):
		db = self.args['sessions']
		sessions = [d for d in os.listdir(db) if os.path.isdir(db)]
		sessions = sorted(sessions)
		if '.DS_Store' in sessions:
			sessions.remove('.DS_Store')
		if 'sessions.json' in sessions:
			sessions.remove('sessions.json')
		self.state['db'] = sessions
		return db

	def makeSessionDirs(self, timestamp):
		folder     = utils.datetime_str(timestamp)
		path       = utils.mkdir(self.args['sessions'], folder)
		transcript = utils.mkdir(path, 'transcript')
		return path, folder

	#################################################
	#	Connection
	#################################################

	def handshake(self):
		self.state['connected'] = True
		return True

	def connected(self):
		return self.state['connected']

	#################################################
	#	Sculpture
	#################################################
		
	def sculptureSetupFromJSON(self, baseModelJSON):
		self.sculpture.reset(self.args['sculpture'])
		baseModel = utils.jsonString2Model(baseModelJSON)
		return self.sculptureSetup(baseModel)

	def sculptureSetup(self, baseModel):
		self.sculpture.reset(self.args['sculpture'])
		setup = self.sculpture.setup({'baseModel': baseModel})
		self.state['session']['sculptures'].append({
			'setup': setup,
			'sculpts': [],
			'hide': False
		})
		self.state['selectedSculpture'] = len(self.state['session']['sculptures'])-1
		self.save()
		return self.state['session']
	
	def sculptureSculpt(self):
		sculpt = self.sculpture.sculpt()
		sculpt['id'] = utils.prepZero(len(self.state['session']['sculptures'])-1) + sculpt['id']
		selected = self.state['selectedSculpture']
		setup_model_name = self.state['session']['sculptures'][selected]['setup']['baseModel']['metadata']['name']
		sculpt['model']['metadata']['name'] = setup_model_name +'_'+ sculpt['id']
		self.state['session']['sculptures'][selected]['sculpts'].append(sculpt)
		self.save()
		return self.state['session']

	def sculptureSelect(self, index):
		index = self.indexInRange(index, self.state['session']['sculptures'])
		self.state['selectedSculpture'] = index
		selectedSculpture = self.state['session']['sculptures'][index]
		self.sculpture.state = selectedSculpture
		self.save()
		return self.state['session']

	def sculptureHide(self, index):
		index = self.indexInRange(index, self.state['session']['sculptures'])
		self.state['session']['sculptures'][index]['hide'] = True
		self.save()
		return self.state['session']

	def sculptureUnhide(self, index):
		index = self.indexInRange(index, self.state['session']['sculptures'])
		self.state['session']['sculptures'][index]['hide'] = False
		self.save()
		return self.state['session']

	def saveInstrument(self, instrument):
		# self.state['session']['instrument'] = instrument # JSON parse?
		# print(instrument)
		# print(json.loads(instrument))
		self.state['session']['instrument'] = json.loads(instrument) # JSON parse?
		self.state['session']['instrumentLog'].append({
			'timestamp': datetime.datetime.now(),
			'instrument': self.state['session']['instrument']
		})
		self.save()
		return self.state['session']

	def indexInRange(self, index, _range):
		indexMax = len(_range)-1
		index = int(index)
		if index > indexMax: index = indexMax
		return index
