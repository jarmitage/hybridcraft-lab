import pandas as pd
import numpy as np

from bokeh.plotting import figure, output_file, output_notebook, show
from bokeh.models import LinearAxis, Range1d
from bokeh.layouts import layout
from bokeh.resources import CDN
from bokeh.embed import file_html
from bokeh.io import export_svgs # some bullshit

def megaPlot(freqData_ref, freqData_iter, interp_ref, interp_iter, mag_interp, mag_scaled, grad_interp, grad_scaled, model, model_mapped):
    p = figure(title="Mega Plot", plot_width=800, plot_height=600, x_axis_label='Frequency', y_axis_label='Magnitude (dB)', x_axis_type="log")
    # p.x_range = Range1d(10, 20000)
    p.x_range = Range1d(np.floor(interp_ref.head(1).index.values[0])-10, np.ceil(interp_ref.tail(1).index.values[0])+200) # trim X to model
    p.y_range = Range1d(-100,10)
    p.extra_y_ranges = {"scaled": Range1d(start=0, end=3)} # Twin Y axis for linearised scale
    p.add_layout(LinearAxis(y_range_name="scaled"), 'right')

    # Analysis data 
    p.line(freqData_ref.index,  freqData_ref['Magnitude (dB)'],  line_width=2, line_color="red",  legend="Analysis Baseline")
    p.line(freqData_ref.index,  freqData_iter['Magnitude (dB)'], line_width=2, line_color="blue", legend="Analysis Iteration")
    p.circle(interp_ref.index,  interp_ref['Magnitude (dB)'],    size=8,       line_color="red",  fill_color="white", legend="Analysis Baseline")
    p.circle(interp_iter.index, interp_iter['Magnitude (dB)'],   size=8,       line_color="blue", fill_color="white", legend="Analysis Iteration")

    # Mag diff -> gain
    mag_scaled = pd.DataFrame(mag_scaled, columns=['Scaled']).set_index(mag_interp.index)
    p.line(mag_interp.index,   mag_interp['Magnitude (dB)'],  line_width=2, alpha=0.5, line_color="darkgreen", legend="Magnitude Diff (dB)")
    p.line(mag_scaled.index,   mag_scaled['Scaled'],          line_width=2, alpha=0.5, line_color="darkgreen", legend="Magnitude Diff (linear)", y_range_name="scaled")
    p.circle(mag_interp.index, mag_interp['Magnitude (dB)'],  size=8,       alpha=0.5, line_color="darkgreen", legend="Magnitude Diff (dB)",     fill_color="white")
    p.circle(mag_scaled.index, mag_scaled['Scaled'],          size=8,       alpha=0.5, line_color="darkgreen", legend="Magnitude Diff (linear)", fill_color="white", y_range_name="scaled")

    # model comparison
    f   = model['resonators']['freq']
    a_g = model['resonators']['gain']
    a_d = model['resonators']['decay']
    b_g = model_mapped['resonators']['gain']
    b_d = model_mapped['resonators']['decay']

    # Gain
    p.line  (f, a_g, line_width=2, line_color="green", line_dash='dashed', alpha=0.5, legend="Gain original",  y_range_name="scaled")
    p.line  (f, b_g, line_width=2, line_color="green", line_dash='solid',  alpha=0.5, legend="Gain mapped",    y_range_name="scaled")
    p.circle(f, a_g, line_width=2, line_color="green", line_dash='dashed', alpha=0.5, legend="Gain original",  fill_color="white", size=8, y_range_name="scaled")
    p.circle(f, b_g, line_width=2, line_color="green", line_dash='solid',  alpha=0.5, legend="Gain mapped",    fill_color="green", size=8, y_range_name="scaled")

    # Grad diff -> decay
    grad_scaled = pd.DataFrame(grad_scaled, columns=['Scaled']).set_index(grad_interp.index)
    p.line(grad_interp.index,   grad_interp['Magnitude (dB)'], line_width=2, alpha=0.5, line_color="orange", legend="Gradient Diff (dB)")
    p.line(grad_scaled.index,   grad_scaled['Scaled'],         line_width=2, alpha=0.5, line_color="orange", legend="Gradient Diff (linear^2)", y_range_name="scaled")
    p.circle(grad_interp.index, grad_interp['Magnitude (dB)'], size=8,       alpha=0.5, line_color="orange", legend="Gradient Diff (dB)",       fill_color="white")
    p.circle(grad_scaled.index, grad_scaled['Scaled'],         size=8,       alpha=0.5, line_color="orange", legend="Gradient Diff (linear^2)", fill_color="white", y_range_name="scaled")

    # Decay
    p.line  (f, a_d, line_width=2, line_color="gold",  line_dash='dashed', alpha=0.5, legend="Decay original", y_range_name="scaled")
    p.line  (f, b_d, line_width=2, line_color="gold",  line_dash='solid',  alpha=0.5, legend="Decay mapped",   y_range_name="scaled")
    p.circle(f, a_d, line_width=2, line_color="gold",  line_dash='dashed', alpha=0.5, legend="Decay original", fill_color="white", size=8, y_range_name="scaled")
    p.circle(f, b_d, line_width=2, line_color="gold",  line_dash='solid',  alpha=0.5, legend="Decay mapped",   fill_color="gold",  size=8, y_range_name="scaled")

    p.legend.location = "top_left"
    p.legend.click_policy="hide"
    return p

def megaPlot2(freqData_ref, freqData_iter, interp_ref, interp_iter, mag_interp, mag_scaled, grad_interp, grad_scaled, model, model_mapped):
    # model comparison
    f   = model['resonators']['freq']
    a_g = model['resonators']['gain']
    a_d = model['resonators']['decay']
    b_g = model_mapped['resonators']['gain']
    b_d = model_mapped['resonators']['decay']

    p1 = figure(title="Mega Plot 1", plot_width=800, plot_height=600, x_axis_label='Frequency', y_axis_label='Magnitude (dB)', x_axis_type="log")
    # p1.x_range = Range1d(10, 20000)
    p1.x_range = Range1d(np.floor(interp_ref.head(1).index.values[0])-10, np.ceil(interp_ref.tail(1).index.values[0])+200) # trim X to model
    p1.y_range = Range1d(-100,0)
    p1.extra_y_ranges = {"diff_data": Range1d(start=-15, end=15)} # Twin Y axis for diff data
    p1.add_layout(LinearAxis(y_range_name="diff_data"), 'right')

    # Analysis data 
    p1.line(freqData_ref.index,  freqData_ref['Magnitude (dB)'],  line_width=2, line_color="red",  legend="Analysis Baseline (left Y)")
    p1.line(freqData_ref.index,  freqData_iter['Magnitude (dB)'], line_width=2, line_color="blue", legend="Analysis Iteration (left Y)")
    p1.circle(interp_ref.index,  interp_ref['Magnitude (dB)'],    size=8,       line_color="red",  fill_color="white", legend="Analysis Baseline (left Y)")
    p1.circle(interp_iter.index, interp_iter['Magnitude (dB)'],   size=8,       line_color="blue", fill_color="white", legend="Analysis Iteration (left Y)")

    # Mag diff -> gain , Grad diff -> decay
    mag_scaled  = pd.DataFrame(mag_scaled, columns=['Scaled']).set_index(mag_interp.index)
    grad_scaled = pd.DataFrame(grad_scaled, columns=['Scaled']).set_index(grad_interp.index)
    p1.line(mag_interp.index,    mag_interp['Magnitude (dB)'],  line_width=2, line_color="darkgreen", legend="Magnitude Diff (dB) (right Y)", y_range_name="diff_data")
    p1.circle(mag_interp.index,  mag_interp['Magnitude (dB)'],  size=8,       line_color="darkgreen", legend="Magnitude Diff (dB) (right Y)", fill_color="white", y_range_name="diff_data")
    p1.line(grad_interp.index,   grad_interp['Magnitude (dB)'], line_width=2, line_color="orange", legend="Gradient Diff (dB) (right Y)", y_range_name="diff_data")
    p1.circle(grad_interp.index, grad_interp['Magnitude (dB)'], size=8,       line_color="orange", legend="Gradient Diff (dB) (right Y)", fill_color="white", y_range_name="diff_data")

    # Plot 2
    p2 = figure(title="Mega Plot 2", plot_width=800, plot_height=600, x_axis_label='Frequency', y_axis_label='Magnitude (dB)', x_axis_type="log")
    # p2.x_range = Range1d(10, 20000)
    p2.y_range = Range1d(0,1)
    p2.extra_y_ranges = {"scaled": Range1d(start=0, end=3)} # Twin Y axis for linearised scale
    p2.add_layout(LinearAxis(y_range_name="scaled"), 'right')

    # model gain + mag diff linearised
    p2.line  (f, a_g, line_width=2, line_color="green", line_dash='dashed', alpha=0.5, legend="Gain original (left Y)")
    p2.line  (f, b_g, line_width=2, line_color="green", line_dash='solid',  alpha=0.5, legend="Gain mapped (left Y)")
    p2.circle(f, a_g, line_width=2, line_color="green", line_dash='dashed', alpha=0.5, legend="Gain original (left Y)",  fill_color="white", size=8)
    p2.circle(f, b_g, line_width=2, line_color="green", line_dash='solid',  alpha=0.5, legend="Gain mapped (left Y)",    fill_color="green", size=8)
    p2.line(mag_scaled.index,   mag_scaled['Scaled'],  line_width=2, line_color="darkgreen", legend="Magnitude Diff (linear) (right Y)", y_range_name="scaled")
    p2.circle(mag_scaled.index, mag_scaled['Scaled'],  size=8,       line_color="darkgreen", legend="Magnitude Diff (linear) (right Y)", fill_color="white", y_range_name="scaled")

    # model decay + grad diff linearised^2
    p2.line  (f, a_d, line_width=2, line_color="gold",  line_dash='dashed', alpha=0.5, legend="Decay original (left Y)")
    p2.line  (f, b_d, line_width=2, line_color="gold",  line_dash='solid',  alpha=0.5, legend="Decay mapped (left Y)")
    p2.circle(f, a_d, line_width=2, line_color="gold",  line_dash='dashed', alpha=0.5, legend="Decay original (left Y)", fill_color="white", size=8)
    p2.circle(f, b_d, line_width=2, line_color="gold",  line_dash='solid',  alpha=0.5, legend="Decay mapped (left Y)",   fill_color="gold",  size=8)
    p2.line(grad_scaled.index,   grad_scaled['Scaled'], line_width=2, line_color="orange", legend="Gradient Diff (linear^2) (right Y)", y_range_name="scaled")
    p2.circle(grad_scaled.index, grad_scaled['Scaled'], size=8,       line_color="orange", legend="Gradient Diff (linear^2) (right Y)", fill_color="white", y_range_name="scaled")

    p1.legend.location = "bottom_left"
    p1.legend.click_policy="hide"
    p2.legend.location = "top_left"
    p2.legend.click_policy="hide"
    return p1, p2

def plotCompareAnalysisAndInterpData(freqData_ref, freqData_iter, interp_ref, interp_iter):
    p = figure(title="Analysis Data", plot_width=800, plot_height=400, x_axis_label='Frequency', y_axis_label='Magnitude (dB)', x_axis_type="log")
    p.x_range = Range1d(10, 20000)
    # p.x_range = Range1d(np.floor(interp_ref.head(1).index.values[0])-10, np.ceil(interp_ref.tail(1).index.values[0])+200) # trim X to model
    p.y_range = Range1d(-100,0)
    p.line(freqData_ref.index,  freqData_ref['Magnitude (dB)'],  line_width=2, line_color="red",  legend="Baseline")
    p.line(freqData_ref.index,  freqData_iter['Magnitude (dB)'], line_width=2, line_color="blue", legend="Iteration")
    p.circle(interp_ref.index,  interp_ref['Magnitude (dB)'],    size=8,       line_color="red",  fill_color="white")
    p.circle(interp_iter.index, interp_iter['Magnitude (dB)'],   size=8,       line_color="blue", fill_color="white")
    p.legend.location = "top_left"
    p.legend.click_policy="hide"
    return p

def plotDiffAndMappingData(interp_diff, interp_shift, scaled):
    interp_diff_scaled = pd.DataFrame(scaled, columns=['Scaled']).set_index(interp_diff.index)
    p = figure(title="Diff and Mapping Data", plot_width=350, plot_height=400, x_axis_label='Frequency', y_axis_label='Magnitude (dB)', x_axis_type="log")
    # p.x_range = Range1d(10, 20000)
    p.y_range = Range1d(-50,50)
    p.extra_y_ranges = {"scaled": Range1d(start=-10, end=10)}
    p.add_layout(LinearAxis(y_range_name="scaled"), 'right')
    p.line(interp_diff.index,          interp_diff['Magnitude (dB)'],  line_width=2, line_color="violet",    legend="Diff")
    p.line(interp_shift.index,         interp_shift['Magnitude (dB)'], line_width=2, line_color="royalblue", legend="+ Mean")
    p.line(interp_diff_scaled.index,   interp_diff_scaled['Scaled'],   line_width=2, line_color="indigo",    legend="Scaled",    y_range_name="scaled")
    p.circle(interp_diff.index,        interp_diff['Magnitude (dB)'],  size=8,       line_color="violet",    fill_color="white")
    p.circle(interp_shift.index,       interp_shift['Magnitude (dB)'], size=8,       line_color="royalblue", fill_color="white")
    p.circle(interp_diff_scaled.index, interp_diff_scaled['Scaled'],   size=8,       line_color="indigo",    fill_color="white", y_range_name="scaled")
    p.legend.location = "bottom_left"
    p.legend.click_policy="hide"
    return p

def plotDiffAndMappingData2(interp_diff, scaled):
    interp_diff_scaled = pd.DataFrame(scaled, columns=['Scaled']).set_index(interp_diff.index)
    p = figure(title="Diff and Mapping Data", plot_width=350, plot_height=400, x_axis_label='Frequency', y_axis_label='Magnitude (dB)', x_axis_type="log")
    # p.x_range = Range1d(10, 20000)
    p.y_range = Range1d(-50,50)
    p.extra_y_ranges = {"scaled": Range1d(start=0, end=15)}
    p.add_layout(LinearAxis(y_range_name="scaled"), 'right')
    p.line(interp_diff.index,          interp_diff['Magnitude (dB)'],  line_width=2, line_color="violet",    legend="Diff (left Y)")
    p.line(interp_diff_scaled.index,   interp_diff_scaled['Scaled'],   line_width=2, line_color="indigo",    legend="Scaled (right Y)",    y_range_name="scaled")
    p.circle(interp_diff.index,        interp_diff['Magnitude (dB)'],  size=8,       line_color="violet",    fill_color="white")
    p.circle(interp_diff_scaled.index, interp_diff_scaled['Scaled'],   size=8,       line_color="indigo",    fill_color="white", y_range_name="scaled")
    p.legend.location = "bottom_left"
    p.legend.click_policy="hide"
    return p

def plotDiffAndMappingData3(interp, scaled):
    scaled = pd.DataFrame(scaled, columns=['Scaled']).set_index(interp.index)
    p = figure(title="Diff and Mapping Data", plot_width=350, plot_height=400, x_axis_label='Frequency', y_axis_label='Magnitude (dB)', x_axis_type="log")
    # p.x_range = Range1d(10, 20000)
    p.y_range = Range1d(-50,50)
    p.extra_y_ranges = {"scaled": Range1d(start=0, end=5)}
    p.add_layout(LinearAxis(y_range_name="scaled"), 'right')
    p.line(interp.index,   interp['Magnitude (dB)'],  line_width=2, line_color="violet",    legend="Diff (left Y)")
    p.line(scaled.index,   scaled['Scaled'],          line_width=2, line_color="indigo",    legend="Scaled (right Y)",    y_range_name="scaled")
    p.circle(interp.index, interp['Magnitude (dB)'],  size=8,       line_color="violet",    fill_color="white")
    p.circle(scaled.index, scaled['Scaled'],          size=8,       line_color="indigo",    fill_color="white", y_range_name="scaled")
    p.legend.location = "top_left"
    p.legend.click_policy="hide"
    return p

def plotModel(a):
    f   = a['resonators']['freq']
    a_g = a['resonators']['gain']
    a_d = a['resonators']['decay']
    p = figure(title=a['metadata']['name'].values[0], plot_width=900, plot_height=600, x_axis_label='Frequency', y_axis_label='Gain/Decay', x_axis_type="log")
    # p.x_range = Range1d(10, 20000)
    # p.y_range = Range1d(0, 1)
    p.line  (f, a_g, line_width=2, line_color="green", line_dash='solid',  alpha=0.5, legend="Gain")
    p.line  (f, a_d, line_width=2, line_color="gold",  line_dash='solid',  alpha=0.5, legend="Decay")
    p.circle(f, a_g, line_width=2, line_color="green", line_dash='solid',  alpha=0.5, fill_color="green", size=8)
    p.circle(f, a_d, line_width=2, line_color="gold",  line_dash='solid',  alpha=0.5, fill_color="gold",  size=8)
    p.legend.location = "top_left"
    p.legend.click_policy="hide"
    return p

def plotCompareModels(a, b):
    f   = a['resonators']['freq']
    a_g = a['resonators']['gain']
    a_d = a['resonators']['decay']
    b_g = b['resonators']['gain']
    b_d = b['resonators']['decay']
    p = figure(title="Original model vs. mapped model", plot_width=350, plot_height=400, x_axis_label='Frequency', y_axis_label='Gain', x_axis_type="log")
    # p.x_range = Range1d(10, 20000)
    # p.y_range = Range1d(0, 1)
    p.line  (f, a_g, line_width=2, line_color="green", line_dash='dashed', alpha=0.5, legend="Original gain")
    p.line  (f, b_g, line_width=2, line_color="green", line_dash='solid',  alpha=0.5, legend="Mapped gain")
    p.line  (f, a_d, line_width=2, line_color="gold",  line_dash='dashed', alpha=0.5, legend="Original decay")
    p.line  (f, b_d, line_width=2, line_color="gold",  line_dash='solid',  alpha=0.5, legend="Mapped decay")
    p.circle(f, a_g, line_width=2, line_color="green", line_dash='dashed', alpha=0.5, fill_color="white", size=8)
    p.circle(f, b_g, line_width=2, line_color="green", line_dash='solid',  alpha=0.5, fill_color="green", size=8)
    p.circle(f, a_d, line_width=2, line_color="gold",  line_dash='dashed', alpha=0.5, fill_color="white", size=8)
    p.circle(f, b_d, line_width=2, line_color="gold",  line_dash='solid',  alpha=0.5, fill_color="gold",  size=8)
    p.legend.location = "top_left"
    p.legend.click_policy="hide"
    return p

def plotCompareModelsGainVsFreq(a, b):
    f   = a['resonators']['freq']
    a_g = a['resonators']['gain']
    a_d = a['resonators']['decay']
    b_g = b['resonators']['gain']
    b_d = b['resonators']['decay']
    p = figure(title="Original model vs. mapped model (gain vs. freq)", plot_width=350, plot_height=400, x_axis_label='Frequency', y_axis_label='Gain', x_axis_type="log")
    # p.x_range = Range1d(10, 20000)
    # p.y_range = Range1d(0, 1)
    p.circle(f, a_g, legend="Original", size=a_d*100, line_width=2, line_color="white", fill_color="red",  alpha=0.5)
    p.circle(f, b_g, legend="Mapped",   size=b_d*100, line_width=2, line_color="white", fill_color="blue", alpha=0.5)
    p.legend.location = "top_right"
    p.legend.click_policy="hide"
    return p

def plotCompareModelsGainVsDecay(a, b):
    f   = a['resonators']['freq']
    a_g = a['resonators']['gain']
    a_d = a['resonators']['decay']
    b_g = b['resonators']['gain']
    b_d = b['resonators']['decay']
    p = figure(title="Original model vs. mapped model (gain vs. decay)", plot_width=350, plot_height=400, x_axis_label='Decay', y_axis_label='Gain')
    # p.x_range = Range1d(10, 20000)
    # p.y_range = Range1d(0, 1)
    p.circle(a_d, a_g, legend="Original", size=8, line_width=2, line_color="white", fill_color="red",  alpha=0.5)
    p.circle(b_d, b_g, legend="Mapped",   size=8, line_width=2, line_color="white", fill_color="blue", alpha=0.5)
    p.legend.location = "top_right"
    p.legend.click_policy="hide"
    return p

def fig_iterationFreqData(freqData_ref, freqData_iter, interp_ref, interp_iter, mag_interp, mag_scaled, grad_interp, grad_scaled, model, model_mapped):
    # model comparison
    f   = model['resonators']['freq']
    a_g = model['resonators']['gain']
    a_d = model['resonators']['decay']
    b_g = model_mapped['resonators']['gain']
    b_d = model_mapped['resonators']['decay']

    p1 = figure(title="Magnitude (dB) vs. Frequency", plot_width=800, plot_height=600, x_axis_label='Frequency', y_axis_label='Magnitude (dB)', x_axis_type="log")
    # p1.x_range = Range1d(10, 20000)
    # p1.x_range = Range1d(np.floor(interp_ref.head(1).index.values[0])-10, np.ceil(interp_ref.tail(1).index.values[0])+200) # trim X to model
    p1.y_range = Range1d(-100,0)
    # p1.extra_y_ranges = {"diff_data": Range1d(start=-15, end=15)} # Twin Y axis for diff data
    # p1.add_layout(LinearAxis(y_range_name="diff_data"), 'right')

    # Analysis data 
    # p1.line(freqData_ref.index,  freqData_ref['Magnitude (dB)'],  line_width=2, line_color="red",  legend="Analysis Baseline (left Y)")
    p1.line(freqData_ref.index,  freqData_iter['Magnitude (dB)'], line_width=2, line_color="black")#, legend="Analysis Iteration (left Y)")
    # p1.circle(interp_ref.index,  interp_ref['Magnitude (dB)'],    size=8,       line_color="red",  fill_color="white", legend="Analysis Baseline (left Y)")
    # p1.circle(interp_iter.index, interp_iter['Magnitude (dB)'],   size=8,       line_color="blue", fill_color="white", legend="Analysis Iteration (left Y)")

    # Mag diff -> gain , Grad diff -> decay
    # mag_scaled  = pd.DataFrame(mag_scaled, columns=['Scaled']).set_index(mag_interp.index)
    # grad_scaled = pd.DataFrame(grad_scaled, columns=['Scaled']).set_index(grad_interp.index)
    # p1.line(mag_interp.index,    mag_interp['Magnitude (dB)'],  line_width=2, line_color="darkgreen", legend="Magnitude Diff (dB) (right Y)", y_range_name="diff_data")
    # p1.circle(mag_interp.index,  mag_interp['Magnitude (dB)'],  size=8,       line_color="darkgreen", legend="Magnitude Diff (dB) (right Y)", fill_color="white", y_range_name="diff_data")
    # p1.line(grad_interp.index,   grad_interp['Magnitude (dB)'], line_width=2, line_color="orange", legend="Gradient Diff (dB) (right Y)", y_range_name="diff_data")
    # p1.circle(grad_interp.index, grad_interp['Magnitude (dB)'], size=8,       line_color="orange", legend="Gradient Diff (dB) (right Y)", fill_color="white", y_range_name="diff_data")

    # p1.legend.location = "bottom_left"
    # p1.legend.click_policy="hide"
    p1.toolbar_location=None
    p1.output_backend="svg"
    return p1

def fig_iterationFreqDataDiff(freqData_ref, freqData_iter, interp_ref, interp_iter, mag_interp, mag_scaled, grad_interp, grad_scaled, model, model_mapped):
    # model comparison
    f   = model['resonators']['freq']
    a_g = model['resonators']['gain']
    a_d = model['resonators']['decay']
    b_g = model_mapped['resonators']['gain']
    b_d = model_mapped['resonators']['decay']

    p1 = figure(title="Magnitude (dB) vs. Frequency", plot_width=800, plot_height=600, x_axis_label='Frequency', y_axis_label='Magnitude (dB)', x_axis_type="log")
    # p1.x_range = Range1d(10, 20000)
    # p1.x_range = Range1d(np.floor(interp_ref.head(1).index.values[0])-10, np.ceil(interp_ref.tail(1).index.values[0])+200) # trim X to model
    p1.y_range = Range1d(-100,0)
    # p1.extra_y_ranges = {"diff_data": Range1d(start=-15, end=15)} # Twin Y axis for diff data
    # p1.add_layout(LinearAxis(y_range_name="diff_data"), 'right')

    # Analysis data 
    # p1.line(freqData_ref.index,  freqData_ref['Magnitude (dB)'],  line_width=2, line_color="red",  legend="Analysis Baseline (left Y)")
    p1.line(freqData_ref.index,  freqData_iter['Magnitude (dB)'], line_width=2, line_color="blue")#, legend="Analysis Iteration (left Y)")
    # p1.circle(interp_ref.index,  interp_ref['Magnitude (dB)'],    size=8,       line_color="red",  fill_color="white", legend="Analysis Baseline (left Y)")
    # p1.circle(interp_iter.index, interp_iter['Magnitude (dB)'],   size=8,       line_color="blue", fill_color="white", legend="Analysis Iteration (left Y)")

    # Mag diff -> gain , Grad diff -> decay
    # mag_scaled  = pd.DataFrame(mag_scaled, columns=['Scaled']).set_index(mag_interp.index)
    # grad_scaled = pd.DataFrame(grad_scaled, columns=['Scaled']).set_index(grad_interp.index)
    # p1.line(mag_interp.index,    mag_interp['Magnitude (dB)'],  line_width=2, line_color="darkgreen", legend="Magnitude Diff (dB) (right Y)", y_range_name="diff_data")
    # p1.circle(mag_interp.index,  mag_interp['Magnitude (dB)'],  size=8,       line_color="darkgreen", legend="Magnitude Diff (dB) (right Y)", fill_color="white", y_range_name="diff_data")
    # p1.line(grad_interp.index,   grad_interp['Magnitude (dB)'], line_width=2, line_color="orange", legend="Gradient Diff (dB) (right Y)", y_range_name="diff_data")
    # p1.circle(grad_interp.index, grad_interp['Magnitude (dB)'], size=8,       line_color="orange", legend="Gradient Diff (dB) (right Y)", fill_color="white", y_range_name="diff_data")

    # p1.legend.location = "bottom_left"
    # p1.legend.click_policy="hide"
    p1.toolbar_location=None
    p1.output_backend="svg"
    return p1
