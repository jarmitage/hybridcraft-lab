from paramiko import SSHClient, SSHConfig, ProxyCommand, Transport
from scp import SCPClient

def scp_bela (host='bbb'):
    ssh_config = SSHConfig()
    ssh_config_file = os.path.expanduser('~/.ssh/config')
    if os.path.exists (ssh_config_file):
        with open (ssh_config_file) as f:
            ssh_config.parse (f)
    bbb = ssh_config.lookup (host)
    sf = Transport ((bbb['hostname'], 22))
    sf.connect (username = bbb['user'])
    sf.auth_none (bbb['user'])
    # progress callback for scp transfer
    # def progress(filename, size, sent, peername):
    #     print("%s:%s %s: %.2f%% \r" % (peername[0], peername[1], filename, float(sent)/float(size)*100))
    # return SCPClient(sf, progress = progress)
    return SCPClient(sf)

def scpModel2Bela(belaName, modelName, project):
    scpb = scp_bela(belaName)
    scpb.put (localModelsPath+'json/'+modelName+'.json', '~/Bela/projects/'+project+'/models/' +'tmp.json')
    scpb.close()
