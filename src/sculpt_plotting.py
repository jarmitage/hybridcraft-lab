import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import IPython.display as ipd

localPlotsPath = '../../../../data/outcomes/plots/'

def plotDecayRateVsFreq (model, model_name, save):
    freq = model['freq']
    gain = model['gain']
    decay = pd.Series(np.exp(-model['decay'] * (1/44100)))
    xlabel, ylabel = 'Decay Rate', 'Frequency'
    xlim, ylim = (0.9995, 1.0), (20,20000)
    fig, axis = plt.subplots(figsize=(12,6))
    axis.barh(freq, decay, fill=False, linestyle="-", linewidth=gain.values*50, edgecolor=(0,0,1,0.5))
    axis.set_yscale('log')
    axis.set_yticks ([10, 100, 1000, 10000])
    axis.set_xlim (xlim)
    axis.set_ylim (ylim)
    axis.set_xlabel (xlabel)
    axis.set_ylabel (ylabel)
    plt.title ('Model \'' + str (model_name) + '\' has ' + str (len (model)) + ' resonances')
    if save == 1:
        axis.get_figure().savefig(localPlotsPath+'decay-vs-freq_'+model_name+'.pdf')

def plotFreqVsGD (model, model_name, save):
    freq = model['freq']
    gain = model['gain']
    decay = pd.Series(np.exp(-model['decay'] * (1/44100)))
    xlabel, ylabel1, ylabel2 = 'Frequency', 'Gain', 'Decay Rate'
    xlim, ylim1, ylim2 = (20,20000), (0, 0.3), (0.9995, 1.0)
    fig, axis1 = plt.subplots(figsize=(13,6))
    # freq vs. gain
    axis1.bar (freq, gain, linewidth=10, edgecolor=(1,0,0,1))
    axis1.set_xscale ('log')
    axis1.set_xticks ([10, 100, 1000, 10000])
    axis1.set_xlabel (xlabel)
    axis1.set_xlim (xlim)
    axis1.set_ylim (ylim1)
    axis1.set_ylabel (ylabel1, color='r')
    axis1.tick_params('y', colors='r')
    # freq vs. decay
    axis2 = axis1.twinx()
    #axis2.plot (freq, decay, 'sb')
    axis2.bar (freq, decay, linewidth=5, edgecolor=(0,0,1,0.25))
    axis2.set_ylim (ylim2)
    axis2.set_ylabel (ylabel2, color='b')
    axis2.tick_params('y', colors='b')
    plt.title ('Model \'' + str (model_name) + '\' has ' + str (len (model)) + ' resonances')
    if save == 1:
        axis1.get_figure().savefig(localPlotsPath+'freq-vs-gd_'+model_name+'.pdf')

def plotFreqVsGDNorm (model, model_name, save):
    freq = model['freq']
    gain = model['gain']
    decay = model['decay']
    # decay = pd.Series(np.exp(-model['Decay'] * (1/44100)))
    xlabel, ylabel1, ylabel2 = 'Frequency', 'Gain', 'Decay Rate'
    xlim, ylim1, ylim2 = (20,20000), (0, 1), (0, 1.0)
    fig, axis1 = plt.subplots(figsize=(13,6))
    # freq vs. gain
    axis1.bar (freq, gain, linewidth=10, edgecolor=(1,0,0,1))
    axis1.set_xscale ('log')
    axis1.set_xticks ([10, 100, 1000, 10000])
    axis1.set_xlabel (xlabel)
    axis1.set_xlim (xlim)
    axis1.set_ylim (ylim1)
    axis1.set_ylabel (ylabel1, color='r')
    axis1.tick_params('y', colors='r')
    # freq vs. decay
    axis2 = axis1.twinx()
    #axis2.plot (freq, decay, 'sb')
    axis2.bar (freq, decay, linewidth=5, edgecolor=(0,0,1,0.25))
    axis2.set_yscale ('log')
    axis2.set_ylim (ylim2)
    axis2.set_ylabel (ylabel2, color='b')
    axis2.tick_params('y', colors='b')
    plt.title ('Model \'' + str (model_name) + '\' has ' + str (len (model)) + ' resonances')
    if save == 1:
        axis1.get_figure().savefig(localPlotsPath+'freq-vs-gd_'+model_name+'.pdf')

def plotCompareAnalysesAndModels(freqData_a, freqData_b, interp_a, interp_b, interp_diff, interp_shift, interp_diff_scaled, model_a, model_b):
    # setup
    xlim, ylim1, ylim2 = (20,20000), (-100, 0), (-50,50)
    xlabel, ylabel = 'Frequency', 'Magnitude (dB)'
    fig, axs = plt.subplots(1,3,figsize=(14,6), gridspec_kw={'width_ratios': [1,0.8,1], 'wspace': 0.35})
    # first plot
    axs[0].plot(freqData_a['Magnitude (dB)'], 'r')
    axs[0].plot(freqData_b['Magnitude (dB)'], 'b')
    axs[0].plot(interp_a['Magnitude (dB)'], 'r', linestyle='None', marker='o')
    axs[0].plot(interp_b['Magnitude (dB)'], 'b', linestyle='None', marker='o')
    axs[0].set_title ('Baseline & comparison measurement')
    axs[0].set_xscale ('log')
    axs[0].set_xlim (xlim)
    axs[0].set_ylim (ylim1)
    axs[0].set_xticks ([10, 100, 1000, 10000])
    axs[0].set_xlabel (xlabel)
    axs[0].set_ylabel (ylabel)
    axs[0].grid(b=True, which='both')
    axs[0].legend (['Baseline', 'Iteration'])
    # second plot
    axs[1].plot(interp_diff['Magnitude (dB)'], 'thistle', linestyle='-', marker='o')
    axs[1].plot(interp_shift['Magnitude (dB)'], 'violet', linestyle='-', marker='o')
    axs[1].set_title ('Differential between baseline & comparison')
    axs[1].set_xscale ('log')
    axs[1].set_xlabel (xlabel)
    axs[1].set_ylim (ylim2)
    axs[1].set_ylabel (ylabel)
    axs[1].legend (['Mag', '+ mean'], loc=2)
    # axs[1].grid(b=True, which='both')
    # second plot twin
    interp_diff_scaled = pd.DataFrame(interp_diff_scaled, columns=['Scaled']).set_index(interp_diff.index)
    axs1_1 = axs[1].twinx()
    axs1_1.plot(interp_diff_scaled, 'indigo', linestyle='-', marker='o')
    axs1_1.set_ylim (-1,1)
    # axs1_1.set_ylabel ('Scaled')
    axs1_1.legend (['Scaled\n(right Y)'], loc=1)
    # third plot
    f   = model_a['resonators']['freq']
    a_g = model_a['resonators']['gain']
    a_d = model_a['resonators']['decay']
    b_g = model_b['resonators']['gain']
    b_d = model_b['resonators']['decay']
    ylabel, xlabel = 'Gain', 'Frequency'
    ylim, xlim = (0, 1.0), (20,20000)
    axs[2].scatter(f, a_g, color=(1,0,0,0.5), s=a_d*200)
    axs[2].scatter(f, b_g, color=(0,0,1,0.5), s=b_d*200)
    axs[2].legend (['Baseline', 'Iteration'])
    axs[2].set_xscale('log')
    # axs[2].set_xticks ([10, 100, 1000, 10000])
    # axs[2].set_xlim (xlim)
    # ax[2].set_ylim (ylim)
    axs[2].set_xlabel (xlabel)
    axs[2].set_ylabel (ylabel)
    axs[2].grid(b=True, which='both')
    axs[2].set_title ('Baseline model vs. mapped model')


def plotCompareAnalyses(freqData_a, freqData_b, interp_a, interp_b, diff):
    xlim, ylim1, ylim2 = (20,20000), (-100, 0), (-50,50)
    xlabel, ylabel = 'Frequency', 'Magnitude (dB)'
    fig, axs = plt.subplots(1,2,figsize=(15,6), gridspec_kw={'width_ratios': [2.5, 1.5]})
    axs[0].plot(freqData_a['Magnitude (dB)'], 'r')
    axs[0].plot(freqData_b['Magnitude (dB)'], 'b')
    axs[0].plot(interp_a['Magnitude (dB)'], 'r', linestyle='None', marker='o')
    axs[0].plot(interp_b['Magnitude (dB)'], 'b', linestyle='None', marker='o')
    axs[0].set_title ('Baseline & comparison measurement')
    axs[0].set_xscale ('log')
    axs[0].set_xlim (xlim)
    axs[0].set_ylim (ylim1)
    axs[0].set_xticks ([10, 100, 1000, 10000])
    axs[0].set_xlabel (xlabel)
    axs[0].set_ylabel (ylabel)
    axs[0].grid(b=True, which='both')
    axs[0].legend (['Baseline', 'Iteration'])
    axs[1].plot(diff['Magnitude (dB)'], 'indigo', linestyle='-', marker='o')
    axs[1].set_title ('Differential between baseline & comparison')
    axs[1].set_xscale ('log')
    axs[1].set_ylim (ylim2)
    axs[1].set_xlabel (xlabel)
    axs[1].set_ylabel (ylabel)
    axs[1].grid(b=True, which='both')

def plotCompareModelGains(a, b):
    f = a['resonators']['freq']
    a_g = a['resonators']['gain']
    a_d = a['resonators']['decay']
    b_g = b['resonators']['gain']
    b_d = b['resonators']['decay']
    ylabel, xlabel = 'Gain', 'Frequency'
    ylim, xlim = (0, 1.0), (20,20000)
    fig, axis = plt.subplots(figsize=(10,6))
    axis.scatter(f, a_g, color=(1,0,0,0.5), s=a_d*200)
    axis.scatter(f, b_g, color=(0,0,1,0.5), s=b_d*200)
    axis.legend (['Baseline', 'Iteration'])
    axis.set_xscale('log')
    axis.set_xticks ([10, 100, 1000, 10000])
    axis.set_xlim (xlim)
#     axis.set_ylim (ylim)
    axis.set_xlabel (xlabel)
    axis.set_ylabel (ylabel)
#     plt.title ('Model \'' + str (model_name) + '\' has ' + str (len (model)) + ' resonances')
#     if save == 1:
#         axis.get_figure().savefig(localPlotsPath+'decay-vs-freq_'+model_name+'.pdf')

def plotComparisonAndDifferential(measurements, interpModelData, differential):
    xlim, ylim1, ylim2 = (20,20000), (-100, 0), (-50,50)
    xlabel, ylabel = 'Frequency', 'Magnitude (dB)'
    fig, axs = plt.subplots(1,2,figsize=(15,6), gridspec_kw={'width_ratios': [2.5, 1.5]})
    axs[0].plot(measurements['Baseline Magnitude (dB)'], 'r')
    axs[0].plot(measurements['Comparison Magnitude (dB)'], 'b')
    axs[0].plot(interpModelData['Baseline Magnitude (dB)'], 'r', linestyle='None', marker='o')
    axs[0].plot(interpModelData['Comparison Magnitude (dB)'], 'b', linestyle='None', marker='o')
    axs[0].set_title ('Baseline & comparison measurement')
    axs[0].set_xscale ('log')
    axs[0].set_xlim (xlim)
    axs[0].set_ylim (ylim1)
    axs[0].set_xticks ([10, 100, 1000, 10000])
    axs[0].set_xlabel (xlabel)
    axs[0].set_ylabel (ylabel)
    axs[0].grid(b=True, which='both')
    axs[0].legend (measurements.columns)
    axs[1].plot(differential, 'indigo', linestyle='-', marker='o')
    axs[1].set_title ('Differential between baseline & comparison')
    axs[1].set_xscale ('log')
    axs[1].set_ylim (ylim2)
    axs[1].set_xlabel (xlabel)
    axs[1].set_ylabel (ylabel)
    axs[1].grid(b=True, which='both')
    axs[1].legend (differential.columns)
