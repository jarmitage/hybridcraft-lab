use AppleScript version "2.4" -- Yosemite (10.10) or later
use scripting additions

-- AppleScript for remote controlling FuzzMeasure

-- If using argv, whole script has to be inside run
on run argv
	-- We're going to `log` to a csv as we go...
	-- First, set DataFrame header / csv column names
	log "Info,Data"
	
	-- Get date & datetime for labelling files
	-- set theDate to item 1 of argv
	-- set theDatetime to item 2 of argv
	-- set stimulusName to item 3 of argv
	-- set dbRoot to item 4 of argv
	
	set measurementPath to item 1 of argv
	
	log "argv 1," & measurementPath -- change to DB path
	-- log "argv 1," & theDate
	-- log "argv 2," & theDatetime
	-- log "argv 3," & stimulusName
	-- log "argv 4," & localPath -- change to DB path
	-- log "argv 4," & dbRoot -- change to DB path
	
	-- Get current time so we can see how long things take
	set currentMillis to "date +%s"
	set startTime to (do shell script currentMillis)
	
	log "starting script," & startTime
	
	-- Get the name of this file
	tell application "Finder"
		set scriptFile to name of (path to me)
	end tell
	
	-- Set paths and filenames
	-- set homePath to (POSIX path of (path to home folder))
	-- set measurementPath to dbRoot & "/apparatus/measurement/"
	-- set fmPath to sculptsPath & "2020/" & theDate
	-- set fmFile to fmPath & "/" & theDatetime & "/" & theDatetime
	-- set fmFileExt to ".fume4"
	
	set stimulusFile to POSIX file (measurementPath & "/stimuli/1s_0.0-20k.wav")
	set recordingFile to POSIX file (measurementPath & "/tmp/tmp_measurement.wav")
	set freqResponseFile to POSIX path of (measurementPath & "/tmp/tmp_freq_response.csv")
	set fmFile to POSIX path of (measurementPath & "/tmp/tmp_fm.fume4")
	
	log "stimulus," & stimulusFile
	log "recording," & recordingFile
	log "freqResponseFile," & freqResponseFile
	
	-- Add path and file info to log
	log "scriptFile," & scriptFile
	-- log "path," & fmPath & "/" & theDatetime
	-- log "file," & theDatetime
	
	-- Open FuzzMeasure if it isn't already
	if application "FuzzMeasure" is not running then
		tell application "FuzzMeasure" to activate
	end if
	
	-- Main test loop
	-- Make a new document and tell it to
	-- Make measurements & graphs, then export the graph data, save itself
	tell application "FuzzMeasure"
		log "making document," & (do shell script currentMillis) - startTime
		make new document
		tell first document
			log "measuring," & (do shell script currentMillis) - startTime
			
			set fieldRecording to import field recording named recordingFile recorded with stimulus signal stimulusFile
			set theMeasurement to item 1 of fieldRecording
			
			log "making graphs," & (do shell script currentMillis) - startTime
			-- make graphs
			set frGraph to make new frequency response graph
			-- set gdGraph to make new group delay graph
			-- set mpGraph to make new mixed phase graph
			-- set irGraph to make new impulse response graph
			-- set hdGraph to make new harmonic distortion graph
			-- set edcGraph to make new energy decay curve graph
			
			log "exporting graph data," & (do shell script currentMillis) - startTime
			-- export graph data
			tell frGraph to export graph data to POSIX file freqResponseFile
			-- tell gdGraph to export graph data to POSIX file ((POSIX path of fmFile) & "_group_delay" & dataFormat)
			-- tell mpGraph to export graph data to POSIX file ((POSIX path of fmFile) & "_mixed_phase" & dataFormat)
			-- tell hdGraph to export graph data to POSIX file ((POSIX path of fmFile) & "_harmonic_distortion" & dataFormat)
			-- tell edcGraph to export graph data to POSIX file ((POSIX path of fmFile) & "_early_decay_curve" & dataFormat)
			
			-- log "exporting impulse response data," & (do shell script currentMillis) - startTime
			-- export impulse response data as audio
			-- export impulse theMeasurement to POSIX file ((POSIX path of fmFile) & "_impulse_response" & audioFormat)
			
			log "saving document," & (do shell script currentMillis) - startTime
			-- save document
			save in POSIX file (POSIX path of fmFile)
			log "closing document," & (do shell script currentMillis) - startTime
			close
		end tell
	end tell
	log "ending script," & (do shell script currentMillis) - startTime
	
end run
