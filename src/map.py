import utils
import pandas as pd
import numpy as np

class Map:
	
	def __init__(self, args):
		self.args = args
		self.state = {
			'flags':  self.args['flags'],
			'params': self.args['params'],
			'inputs': {
				'model': None, 'setup': None, 'sculpt': None
			},
			'process': {
				'interp': { 'setup': None, 'iter': None },
				'mag':  { 'diff': None, 'interp': None, 'scaled': None },
				'grad': { 'diff': None, 'interp': None, 'scaled': None },
			},
			'outputs': { 'model_mapped': None }
		}

	def process(self, model, setup, sculpt):
		inputs = self.updateInputs(model, setup, sculpt)
		interp = self.interpolateMeasurements(model, setup, sculpt)
		model  = self.mapModel(model, setup, sculpt)
		return self.state['outputs']['model_mapped']

	############################################
	#	Mapping
	############################################

	def mapModel(self, model, setup, sculpt):
		mag_scaled  = self.mapModelGains(model, setup, sculpt)
		grad_scaled = self.mapModelDecays(model, setup, sculpt)
		# Map scalars to model
		model_mapped = self.scaleModelGainsByAnalysisMagnitudes(model, mag_scaled,  self.state['params']['gain_scalar'])
		model_mapped = self.scaleModelDecaysByAnalysisGradient (model, grad_scaled, self.state['params']['decay_scalar'])
		model_mapped = self.clipModel(model_mapped)
		self.state['outputs']['model_mapped'] = model_mapped
		return model_mapped

	def mapModelGains(self, model, setup, sculpt):
		diff   = self.diffMeasurements(setup, sculpt)
		interp = self.indexMeasurementByFreqs(self.interpolateMeasurement(model, diff))
		scaled = utils.db2mag(interp['Magnitude (dB)'].values)
		self.state['process']['mag'] = { 'diff': diff, 'interp': interp, 'scaled': scaled }
		return scaled

	def mapModelDecays(self, model, setup, sculpt):
		diff   = self.analysisMagGradientDiff(setup, sculpt)
		interp = self.indexMeasurementByFreqs(self.interpolateMeasurement(model, diff))
		scaled = utils.db2mag(interp['Magnitude (dB)'].values)
		scaled = scaled ** 2
		self.state['process']['grad'] = { 'diff': diff, 'interp': interp, 'scaled': scaled }
		return scaled

	def analysisMagGradientDiff(self, setup, sculpt):
		a_freqs    = self.indexMeasurementByFreqs(setup)
		b_freqs    = self.indexMeasurementByFreqs(sculpt)
		self.state['process']['a_freqs'] = a_freqs
		setupGrad1 = self.analysisMagGradient(a_freqs)
		iterGrad1  = self.analysisMagGradient(b_freqs)
		setupGrad2 = self.analysisMagGradient(self.indexMeasurementByFreqs(setupGrad1))
		iterGrad2  = self.analysisMagGradient(self.indexMeasurementByFreqs(iterGrad1))
		gradDiff1  = self.diffMeasurements(iterGrad1, setupGrad1)
		gradDiff2  = self.diffMeasurements(iterGrad2, setupGrad2)
		return gradDiff2

	def analysisMagGradient(self, freq_data, order=1):
		refIndexed = self.reindexMeasurement(freq_data)
		refIndexed['Magnitude (dB)'] = np.gradient(freq_data['Magnitude (dB)'].values, edge_order=order)
		return refIndexed

	@staticmethod
	def diffMeasurements(setup, sculpt):
		diff = -(setup - sculpt) # diff = (a - b)
		diff['Frequency (Hz)'] = setup['Frequency (Hz)']
		return diff

	############################################
	#	Interpolation
	############################################

	def interpolateMeasurements(self, model, setup, sculpt):
		self.state['process']['interp'] = {
			'setup': self.interpolateMeasurement(model, setup),
			'iter':  self.interpolateMeasurement(model, sculpt)
		}
		return self.state['process']['interp']

	def interpolateMeasurement(self, model, measurement):
		# Takes a model DataFrame and measurement data
		# Interpolates measurement data for the model's specific frequencies
		modelFreqsSeries = pd.Series(model['resonators']['freq'])
		modelFreqsDf     = pd.DataFrame(columns=measurement.columns)
		modelFreqsDf['Frequency (Hz)'] = modelFreqsSeries
		# TODO: Improve interpolation
		interpolated = measurement.append(modelFreqsDf, ignore_index=True).sort_values(by=['Frequency (Hz)']).interpolate()
		filtered = interpolated.sort_index().loc[len(interpolated)-(len(modelFreqsDf)):len(interpolated)-1].reset_index(drop=True)
		return filtered
		# return indexMeasurementByFreqs(filtered) # Df of magnitudes with frequencies as index

	############################################
	#	Scaling
	############################################

	@staticmethod
	def scaleModelGainsByAnalysisMagnitudes(model, mag, scalar=0.15):
		model_gains  = model['resonators']['gain'].values
		model_mapped = model['resonators']
		model_mapped['gain'] = model_gains * mag # scale first by magnitude
		model_mapped['gain'] = model_mapped['gain'] + (scalar * (mag-1)) # mag diff biased at 0, scaled, added
		return {'metadata': model['metadata'], 'resonators': model_mapped}

	@staticmethod
	def scaleModelDecaysByAnalysisGradient(model, grad, scalar=0.2):
		model_decays = model['resonators']['decay'].values
		model_mapped = model['resonators']
		model_mapped['decay'] = model_decays * (1/grad) # scale first by 1/grad
		model_mapped['decay'] = model_mapped['decay'] - (scalar * (grad-1)) # grad diff biased at 0, scaled, added
		return {'metadata': model['metadata'], 'resonators': model_mapped}

    ############################################
	#	Misc
	############################################

	def updateInputs(self, model, setup, sculpt):
		self.state['inputs'] = {
			'model': model, 'setup': setup, 'sculpt': sculpt
		}
		return self.state['inputs']


	@staticmethod
	def clipModel(model):
		model['resonators'] = model['resonators'].clip(lower=0.01) 
		return model

	@staticmethod
	def indexMeasurementByFreqs(df):
		# return dataframe of magnitudes with frequencies as index
		return pd.DataFrame(df['Magnitude (dB)'].values, columns=['Magnitude (dB)']).set_index(df['Frequency (Hz)'])

	@staticmethod
	def reindexMeasurement(_df):
		# return dataframe with regular index and freqs + magnitudes columns
		df = pd.DataFrame(_df.index, columns=['Frequency (Hz)'])
		df['Magnitude (dB)'] = _df['Magnitude (dB)'].values
		return df
