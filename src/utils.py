import sys, os
import math
import pandas as pd
import numpy as np
import scipy.io.wavfile as spiowav
import time
import datetime
import subprocess
import json

localModelsPath = ''

def coll2df (file):
    df = pd.read_csv(file, sep=",",index_col=0,header=None) # load model
    df[1] = df[1].str.strip() # strip whitespace
    df = pd.DataFrame(df[1].str.split(' ',n=3,expand=True)) # split values into columns
    df.index.name = '' # remove index name
    df.columns = ['freq','gain','decay'] # name columns
    df['decay'] = df['decay'].str[:-1] # remove `;` from Decay column values
    df = df.apply(pd.to_numeric) # convert from string to float
    return df

def model2coll (models_path, model_file, model_data):
    pdListMessage  = ' ' + model_data['freq'].astype(str)
    pdListMessage += ' ' + model_data['gain'].astype(str)
    pdListMessage += ' ' + model_data['decay'].astype(str) + ';'
    pdModel = pd.Series(pdListMessage.values)
    pdModel.to_csv(models_path +'/'+ model_file +'.coll', header=False)

def csv2df (file):
    df = pd.read_csv(localModelsPath+'csv/'+file, sep=',', header=None)
    df.columns = ['freq','gain','decay']
    return df

def df2csv (df, file):
    df.to_csv('../data/models/csv/'+file, header=False, index=False)

def model2json(models_path, model_file, model_data):
    metadata = model_data["metadata"].T.to_json()
    metadata = metadata[0:len(metadata)-1]+','
    resonators = '"resonators": '+model_data["resonators"].to_json(orient='records')+'}'
    json_str = metadata+resonators
    # json_str = json.dumps(json_str, indent=4, sort_keys=True) # pretty print..?
    json_file = open(models_path +'/'+ model_file +'.json', "w")
    json_file.write(json_str)
    json_file.close()
    return json_str 

def model2json(model_data):
    metadata = model_data["metadata"].T.to_json()
    metadata = metadata[0:len(metadata)-1]+','
    resonators = '"resonators": '+model_data["resonators"].to_json(orient='records')+'}'
    return metadata+resonators

def model2dict(model_data):
    metadata = model_data["metadata"].T.to_dict(orient='dict')
    metadata = metadata["metadata"]
    resonators = model_data["resonators"].to_dict(orient='records')
    return {"metadata": metadata, "resonators": resonators}

def dict2model(model_dict):
    metadata = pd.DataFrame.from_dict(model_dict['metadata'], orient='index').T
    metadata.index = ['metadata']
    return {
        'metadata':   metadata,
        'resonators': pd.DataFrame.from_dict(model_dict['resonators'])
    }

def json2model(models_path, model_file):
    name, ext = os.path.splitext(model_file)
    if (ext == '.json'):
        data = json.load(open(models_path+'/'+model_file))
    else:
        data = json.load(open(models_path+'/'+model_file+'.json'))
    metadata = pd.DataFrame(data['metadata'],index=['metadata'])
    resonators = pd.DataFrame(data['resonators'])
    cols = resonators.columns.tolist()
    resonators = resonators[cols[1:] + cols[:-2]]
    model = {"metadata": metadata, "resonators": resonators}
    return model

def jsonString2Model(string):
    data = json.loads(string)
    return jsonData2model(data)

def jsonData2model(data):
    metadata = pd.DataFrame(data['metadata'],index=['metadata'])
    resonators = pd.DataFrame(data['resonators'])
    cols = resonators.columns.tolist()
    resonators = resonators[cols[1:] + cols[:-2]]
    model = {"metadata": metadata, "resonators": resonators}
    return model    

def metadata2df(name, fundamental, resonators):
    return pd.DataFrame([[name,fundamental,resonators]],index=["metadata"],columns=["name","fundamental","resonators"])

def modelGen (freq, gain, decay, resonators):
    res = pd.DataFrame ([],np.arange (0, resonators), ['freq', 'gain', 'decay'] ).fillna (0)
    res['freq'], res['gain'], res['decay'] = freq, gain, decay
    for i in range (0, resonators):
        res['freq'][i]  += np.random.randint (0, 100) + ((i*i)*10+np.random.randint(0,5))
        res['gain'][i]  += np.random.uniform (0, 0.1)
        res['decay'][i] += np.random.uniform (0, 2.0)
    return res

def binary2df (path, dt, sep=""):
    return pd.DataFrame (np.fromfile (path, dtype=dt, sep=sep))

def text2df (path, skip=None, sep=""):
    return pd.read_csv (path, sep=sep, skiprows=skip)

def log2wav (path, samplerate, values):
    spiowav.write (path, samplerate, values)

def normDfInRange (df, a, b):
    if (df.max().values[0] > abs(df.min().values[0])):
        a = df.min() / df.max()
    else:
        b = -df.max() / df.min()
    return (b - a) * ( (df - df.min()) / (df.max() - df.min()) ) + a

def mapDf(x, in_min, in_max, out_min, out_max):
    return pd.DataFrame((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

def modelNormalize (model, max_gain, max_decay):
    return pd.concat([model['freq'], mapDf(model['gain'], 0, max_gain, 0, 1), mapDf(model['decay'], 0, max_decay, 0, 1)], axis=1)

def db2mag(db):
    return 10**(db/20)

## Date strings

# def date_str():
#     return datetime.datetime.now().strftime("%Y-%m-%d")

# def datetime_str():
#     return datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")

def date_str(dt):
    return dt.strftime("%Y-%m-%d")

def datetime_str(dt):
    return dt.strftime("%Y-%m-%d_%H%M%S")

## Paths

def getPathTail(path, level):
    sep       = '/'
    split     = path.split(sep)
    path_tail = sep.join(split[len(split)-level:len(split)])
    return path_tail

def getPathHead(path, level):
    sep       = '/'
    split     = path.split(sep)
    path_head = sep.join(split[0:level+1])
    return path_head

def trimPathTail(path, level):
    sep       = '/'
    split     = path.split(sep)
    path_head = sep.join(split[0:len(split)-level-1])
    return path_head

## Base Models

def loadBaseModel(path, modelName):
    return json2model(path, modelName)

def loadBaseModels(path):
    json_file = open(path)
    baseModels = json.load(json_file)
    return baseModels

## Folder

def mkdir(path, name):
    folder_path = path + '/' + name + '/'
    os.mkdir(path + '/' + name + '/')
    return os.path.relpath(folder_path)

def prepZero(num):
    if (num < 10):
        num = '0'+str(num)
    return str(num)
