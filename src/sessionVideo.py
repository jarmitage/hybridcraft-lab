import sys , os
import ffmpeg
import time
import datetime
from tqdm.notebook import tqdm

class SessionVideo:

	def __init__(self, paths):
        self.paths = paths
        self.ffmpeg_datetime_fmt='%Y-%m-%dT%H:%M:%S.000000Z'

	##################################
    # Video processing
    ##################################

    def loadSessionVideoClips(self, session_media_path, source):
        if (source=='raw'):
            session_media_path = self.paths['media']+session_media_path+'raw/'
        elif (source=='cut'):
            session_media_path = self.paths['media']+session_media_path+'cut/'
        else:
            return 0
        clips_paths = [f for f in os.listdir(session_media_path) if os.path.isfile(os.path.join(session_media_path, f))]
        clips_paths = sorted(clips_paths)
        clips_info = pd.DataFrame(columns=['absolute_path','relative_path','file_name','start_time','duration', 'end_time'])
        for i, clip in enumerate(clips_paths):
            name, ext = os.path.splitext(clip) # avoid .DS_Store
            start_time, duration, end_time = np.nan, np.nan, np.nan
            if (ext!=''):
                probe = ffmpeg.probe(session_media_path+clips_paths[i])
                video_info = next(s for s in probe['streams'] if s['codec_type'] == 'video')
                if (source=='raw'):
                    start_time = datetime.datetime.strptime(video_info['tags']['creation_time'], self.ffmpeg_datetime_fmt)
                    duration = video_info['duration']
                    end_time = start_time + datetime.timedelta(seconds=int(float(duration)))
                clips_info.loc[i] = pd.Series({
                    'absolute_path': os.path.abspath(session_media_path),'relative_path': session_media_path,'file_name': clip,
                    'start_time':start_time, 'duration':duration, 'end_time':end_time
                })
        return clips_info

    def trimVideosToSculpts(self, videos, sequence, show_progress=True):
        seq_timestamps = self.sequenceTimestampsAsDatetimes(sequence)
        if (show_progress==True):
            with tqdm(total=len(videos), desc='Videos') as pbar1:
                for i, vid in videos.iterrows():
                    with tqdm(total=len(sequence), desc='Sculpts') as pbar2:
                        for j, sculpt in enumerate(seq_timestamps):
                            pbar2.update(1)
                            if (vid['start_time'] < sculpt < vid['end_time']):
                                if (j < len(seq_timestamps)-1):
                                    self.trimVideoToSculpt(vid, sequence[j], sculpt, seq_timestamps[j+1])
                                else:
                                    self.trimVideoToSculpt(vid, sequence[j], sculpt, vid['end_time'])
                        pbar1.update(1)
        else:
            for i, vid in videos.iterrows():
                for j, sculpt in enumerate(seq_timestamps):
                    if (vid['start_time'] < sculpt < vid['end_time']):
                        if (j < len(seq_timestamps)-1):
                            self.trimVideoToSculpt(vid, sequence[j], sculpt, seq_timestamps[j+1])
                        else:
                            self.trimVideoToSculpt(vid, sequence[j], sculpt, vid['end_time'])

    def trimVideoToSculpt(self, vid, sculpt, sculpt_timestamp, next_sculpt_timestamp):
        source = vid['relative_path']+vid['file_name']
        start_delta = self.timedeltaToHMSFmt(sculpt_timestamp - vid['start_time'])
        end_delta   = self.timedeltaToHMSFmt(next_sculpt_timestamp - vid['start_time'])
        destination = self.trimPathTail(vid['relative_path'], 1)+'/cut/'+sculpt.iloc[1]['Data']+'_sculpt.mp4'
        self.trimVideo(source, start_delta, end_delta, destination)
      
    @staticmethod
    def trimVideo(source, start_delta, end_delta, destination):
        try:
            (
                ffmpeg
                .input(source, ss=start_delta, t=end_delta)
                .output(destination)# iPhone raw: -vcodec h264 -acodec aac)
                .overwrite_output()
                .run(capture_stdout=True, capture_stderr=True)
            )
        except ffmpeg.Error as e:
            print('Handcraft.trimVideo() ffmpeg.Error:', e.stderr.decode(), file=sys.stderr)

    def sequenceTimestampsAsDatetimes(self, sequence):
        timestamps = []
        for i, sculpt in enumerate(sequence):
            timestamps.append(self.sculptTimestampAsDatetime(sculpt))
        return timestamps

    @staticmethod
    def sculptTimestampAsDatetime(sculpt):
        sculpt_dt_fmt ='%Y-%m-%d_%H%M%S'
        return datetime.datetime.strptime(sculpt.iloc[1]['Data'], sculpt_dt_fmt)

    @staticmethod
    def timedeltaToHMSFmt(delta):
        delta = str(delta)
        return delta[7:len(delta)]
