import datetime
import json
import pandas as pd

import utils
from measure import Measure 
from map import Map

class Sculpture:
	
	def __init__(self, args):
		self.reset(args)

	def reset(self, args):
		self.args = args
		self.state = {
			'setup': {
				'baseModel':    None,
				'timestamp':    None,
				'freqResponse': {}
			},
			'sculpts': [{
				'id':           '',
				'timestamp':    None,
				'freqResponse': {},
				'diff':         {},
				'model':        {}
			}],
			'hide': False
		}
		self.measure = Measure(self.args['measure'])
		self.map = Map(self.args['map'])

	def setup(self, settings):
		freqResponse = self.measure.process()
		self.state['setup'] = {
			# 'baseModel':    utils.model2dict(utils.loadBaseModel(self.args['baseModels'], settings['baseModel'])),
			'baseModel':    utils.model2dict(settings['baseModel']),
			'timestamp':    datetime.datetime.now(),
			'freqResponse': freqResponse.to_dict()
		}
		return self.state['setup']

	def sculpt(self):
		freqResponse = self.measure.process()
		model = self.map.process(
			utils.dict2model(self.state['setup']['baseModel']), 
			pd.DataFrame.from_dict(self.state['setup']['freqResponse']),
			pd.DataFrame.from_dict(freqResponse)
		)
		self.state['sculpts'].append({
			'id':           utils.prepZero(len(self.state['sculpts'])-1),
			'timestamp':    datetime.datetime.now(),
			'freqResponse': freqResponse.to_dict(),
			'diff':         self.map.state['process']['mag']['diff'].to_dict(),
			'model':        utils.model2dict(model)
		})
		index = len(self.state['sculpts'])-1
		return self.state['sculpts'][index]
