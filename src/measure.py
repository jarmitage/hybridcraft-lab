import os
import pandas as pd
import sounddevice as sd
import time
import scipy.io.wavfile as spiowav

import utils

class Measure:
	
	# args = {
	# 	'root':    root,
	# 	'tmp':     root+'tmp/tmp_measurement.wav',
	# 	'freq':    root+'tmp/tmp_freq_response.csv',
	# 	'device':  ('io|2', 'io|2'),
	# 	'stimuli': root+'stimuli/1s_0.1-20k.wav',
	# 	'script':  src_root+'measure.applescript'
	# }
	
	def __init__(self, args):
		self.args = args
		self.state = {
			'measurement': {},
			'analysisLog': {},
			'freqResponse': {}
		}

	def process(self):
		self.state['measurement']  = self.playRecStimuli()
		self.state['analysisLog']  = self.analyseMeasurement()
		self.state['freqResponse'] = self.loadFreqResponse()
		return self.state['freqResponse']

	def playRecStimuli(self):
		fs, stimuli = spiowav.read(self.args['stimuli'])
		sd.default.device = self.args['device'] 
		sd.default.samplerate = fs
		duration = len(stimuli) / fs # get duration of stimuli
		measurement = sd.playrec(stimuli, fs, channels=1) # output stimuli and record input
		time.sleep(duration*1.2) # wait for recording to finish
		spiowav.write(self.args['tmp'], fs, measurement) # write to tmp location
		return measurement

	def analyseMeasurement(self):
		analysisLog = self.appleScriptToDf(self.args['script'], os.path.abspath(self.args['root']))
		return analysisLog

	def loadFreqResponse(self):
		return pd.read_csv(open(self.args['freq'])).drop(['Unnamed: 2'], axis=1)

	def appleScriptToDf(self, script, args):
	    return self.appleScriptLogToDf(self.runAppleScript(script, args))

	@staticmethod
	def runAppleScript(script, args):
	    return get_ipython().getoutput("{'osascript ' + script + ' ' + args}")

	@staticmethod
	def appleScriptLogToDf(log):
	    df = pd.DataFrame([sub.split(",") for sub in log])
	    return df.rename(columns=df.iloc[0]).drop(df.index[0])

	@staticmethod
	def quitApp(application):
	    get_ipython().system("{'osascript -e quit app' + application}")
